// inclusão de bibliotecas.    
#include <Servo.h>    // inclui biblioteca de manipulação de servos motores.    
#include <Ultrasonic.h> // inclui biblioteca de manipulação do sensor ultrassônico.    
#include <AFMotor.h>   // inclui biblioteca de manipulação de motores DCs.  
    

#define SERVO_MOTOR_CABECA_PIN 10 //Define o mini servo motor ligado no pino 10. 
#define echoPin 13 //Pino 13 recebe o pulso do echo
#define trigPin 12 //Pino 12 envia o pulso para gerar o echo
#define COEFICIENTE_DISTANCIA 58 //Coeficiente da distancia do sonar
#define HC_SR04_TRIGGER A2 // Define o pino do Trigger do sensor ultrassônico no pino ANALÓGICO A2  
#define HC_SR04_ECHO A3   // Define o pino do Echo do sensor ultrassônico no pino ANALÓGICO A3  
#define VELOCIDADE_MOTOR 110 //Define a velocidade dos dois motores

//Graus do servo motor da cabeça
#define CABECA_NO_MEIO 90
#define CABECA_PARA_ESQUERDA 150
#define CABECA_PARA_DIREITA 30

AF_DCMotor motorDir(3);    // Define o motor da direita ligado ao M3  
AF_DCMotor motorEsq(4);    // Define o motor da esquerda ligado ao M4  
Servo servo_ultra_sonico; // nomeando o servo motor
    
void setup(){
  Serial.begin(9600); //inicia a porta serial
  servo_ultra_sonico.attach(SERVO_MOTOR_CABECA_PIN);   // Define o mini servo motor ligado no pino.    
  pinMode(HC_SR04_TRIGGER, OUTPUT); // Define o trigger do sensor para enviar o sinal  
  pinMode(HC_SR04_ECHO, INPUT);   // Define o Echo do sensor para receber o sinal  
  motorDir.setSpeed(VELOCIDADE_MOTOR);       // Define a velocidade para os motores 1.A velocidade máxima é 255  
  motorEsq.setSpeed(VELOCIDADE_MOTOR);       // Define a velocidade para os motores 2. A velocidade máxima é 255  
  servo_ultra_sonico.write(CABECA_NO_MEIO);   // O servo do sensor ultrasonico se inicia a 90 graus (meio)    
  parar();       // inicia o motor parado
  
}
 
void loop(){

}

/**
* Calcula a distancia entre o robo e um objeto de colisao
**/
int distancia() {
   digitalWrite(HC_SR04_TRIGGER, LOW);     // Desliga a emisão do som  
   delayMicroseconds(4);            // Aguarda 4 segundos  
   digitalWrite(HC_SR04_TRIGGER, HIGH);     // Liga a trasmisão de som  
   delayMicroseconds(20);            // Continua emitindo o som durante 20 segundos  
   digitalWrite(HC_SR04_TRIGGER, LOW);     // Desliga a emisão do som   
   delayMicroseconds(10);            // Aguarda 10 segundos para poder receber o som  
   long pulse_us = pulseIn(HC_SR04_ECHO, HIGH); // Liga o recebedor e calcula quandos pulsos ele recebeu  
   int distancia_cm = (pulse_us / COEFICIENTE_DISTANCIA);        // Calcula a distaâcia em CM  
   delay(300);  
   return distancia_cm;             // Retorna a distância
}


//SERVO MOTOR CABEÇA
void virarCabecaParaDireita() {
    servo_ultra_sonico.write(CABECA_PARA_DIREITA);
}

void virarCabecaParaEsquerda() {
    servo_ultra_sonico.write(CABECA_PARA_ESQUERDA);
}

void cabecaNoMeio() {
    servo_ultra_sonico.write(CABECA_NO_MEIO);
}

//SONAR
int calcularDistanciaCentro(){    
   cabecaNoMeio();
   delay(200);   
   int leituraDoSonar = distancia();  // Ler sensor de distância  
   delay(600);   
   leituraDoSonar = distancia();   
   delay(600);   
   Serial.print("Distancia Centro: "); // Exibe no serial  
   Serial.println(leituraDoSonar);   
   return leituraDoSonar;       // Retorna a distância  
}  

int calcularDistanciaDireita(){    
   virarCabecaParaDireita();
   delay(200);   
   int leituraDoSonar = distancia();  // Ler sensor de distância  
   delay(600);   
   leituraDoSonar = distancia();   
   delay(600);   
   Serial.print("Distancia Direita: "); // Exibe no serial  
   Serial.println(leituraDoSonar);   
   return leituraDoSonar;       // Retorna a distância  
} 

int calcularDistanciaEsquerda(){    
   virarCabecaParaEsquerda();
   delay(200);   
   int leituraDoSonar = distancia();  // Ler sensor de distância  
   delay(600);   
   leituraDoSonar = distancia();   
   delay(600);   
   Serial.print("Distancia Esquerda: "); // Exibe no serial  
   Serial.println(leituraDoSonar);   
   return leituraDoSonar;       // Retorna a distância  
} 



//MOTOR
void parar() {
   Serial.println("=====> Parado");
   motorDir.run(RELEASE); 
   motorEsq.run(RELEASE); 
}


void frente() {
   Serial.println("=====> Andando");
   motorDir.run(FORWARD); 
   motorEsq.run(FORWARD); 
   delay(500);
}

void direita() {    
   Serial.println("====> Direita ");  
   motorDir.run(FORWARD); // Roda vai para frente  
   motorEsq.run(BACKWARD); // Roda vai para trás   
   delay(100);    
}    
  
// Função que faz o robô virar à esquerda    
void esquerda() {    
   Serial.println("===> Esquerda ");  
   motorDir.run(BACKWARD); // Roda vai para trás  
   motorEsq.run(FORWARD); // Roda vai para frente  
   delay(100);    
}  

void tras() {
   Serial.println("=====> Robo dando Re");
   motorDir.run(BACKWARD); // Roda vai para trás   
   motorEsq.run(BACKWARD); // Roda vai para trás   
   delay(100);    
}
