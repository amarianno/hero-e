#include <SoftwareSerial.h>
#include <VoiceRecognitionV3.h>
#include <LiquidCrystal.h>

/*==============================================================================
 * INSTANCIANDO VARIAVEIS
 *============================================================================*/
/**
 * Arduino    VoiceRecognitionModule
 * 8   ------->     TX
 * 9   ------->     RX
 */
VR myVR(8,9);
//LCD
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

/*==============================================================================
 * RECONHECIMENTO DE VOZ
 *============================================================================*/

uint8_t records[7]; // save record
uint8_t buf[64];

#define vozHeroe        (20)

//GRUPO MENU
#define vozSaudacao     (1)
#define vozTruques      (5)
#define vozPergunta     (10)
#define vozManual       (12)
#define vozMusica       (15)

//GRUPO SAUDAÇAO
#define vozCasa         (0)
#define vozNome         (2)
#define vozBomDia       (3)
#define vozBoaNoite     (4)

//GRUPO TRUQUES
#define vozMoonWalker   (6)
#define vozPisca        (7)
#define vozDormir       (8)
#define vozSelfie       (9)

//GRUPO PERGUNTA
#define vozTemperatura  (11)

//GRUPO MANUAL
#define vozParar        (13)
#define vozAutonomo     (14)

/*==============================================================================
 * Exibe no LCD uma mensagem de até 16 caracteres
 * msg - a mensagem
 * coluna - coluna do inicio da mensagem 0 a 16
 * linha - linha da mensagem 0 ou 1
 *============================================================================*/

void mensagemLCD(char msg[], int coluna, int linha) {
  lcd.setCursor(coluna, linha);
  lcd.print(msg);
}

/*==============================================================================
 * Limpa o LCD para novas mensagens
 *============================================================================*/

void limparLCD() {
  lcd.clear();
}

/*==============================================================================
 * Aguarda X milisegundos
 *============================================================================*/
void esperar(int milisegundos) {
  delay(milisegundos);
}

/*==============================================================================
 * Carrega os grupos de comandos de voz
 *============================================================================*/
void carregarGrupo(char nomeGrupo[], int qtde) {
  if(myVR.load(records, qtde) >= 0){
    mensagemLCD("Grupo ", 1 ,0);
    mensagemLCD(nomeGrupo, 7 ,0);
    mensagemLCD("Carregado...", 3 ,1);
  }
}
/*==============================================================================
 * CArrega comandos de voz de acordo com o item do menu
 *============================================================================*/
void carregarGrupoComandoVoz(int ret) {

  limparLCD();

  if(buf[1] == vozTruques) {
    records[0] = vozHeroe;
    records[1] = vozMoonWalker;
    records[2] = vozPisca;
    records[3] = vozDormir;
    records[4] = vozSelfie;

    carregarGrupo("TRUQES", 5);

  }
  else if(buf[1] == vozSaudacao) {
    records[0] = vozHeroe;
    records[1] = vozCasa;
    records[2] = vozNome;
    records[3] = vozBomDia;
    records[4] = vozBoaNoite;

    carregarGrupo("SAUDACAO", 5);

  }
  else if(buf[1] == vozHeroe) {
    records[0] = vozSaudacao;
    records[1] = vozTruques;
    records[2] = vozPergunta;
    records[3] = vozManual;
    records[4] = vozMusica;

    carregarGrupo("MENU", 5);

  }
  else if(buf[1] == vozCasa) {
    mensagemLCD("Bem vindo ", 1, 0);
    mensagemLCD("Em nossa casa", 1, 1);
  }
  else if(buf[1] == vozBomDia) {
    mensagemLCD("Tenha um", 1, 0);
    mensagemLCD("Bom dia", 1, 1);
  }
  else if(buf[1] == vozBomDia) {
    mensagemLCD("Tenha uma", 1, 0);
    mensagemLCD("Boa noite", 1, 1);
    //Desliga os LEDs dos olhos
    //para os sons
  }
}


/*==============================================================================
 * Inicia as variaveis do arduino
 *============================================================================*/

void setup()
{
  /** Inicializando os componentes */
  myVR.begin(9600);
  lcd.begin(16, 2);

  // Inicia mandando uma mensagem para o LCD.
  mensagemLCD("Iniciando", 1, 0);
  mensagemLCD("HERO-E...", 5, 1);
  esperar(5000);
  limparLCD();

  //carrega na memoria o comando para ativar o
  //menu de comandos de voz
  myVR.load((uint8_t) vozHeroe);
}

/*==============================================================================
 * Loop que acontece durante toda a vida do arduino
 *============================================================================*/

void loop() {

  int ret;
  ret = myVR.recognize(buf, 50);
  if(ret > 0){
    carregarGrupoComandoVoz(ret);
  }
}
